#include <iostream>

class Stack{
private:
    int size = 0;
    int* massiv = new int[0];
public:
    ~Stack(){
        delete[] massiv;
    }
    int pop(){
        if(size>0) {
            size--;
            int rez = massiv[size];
            int *temp = new int[size];
            for (int i = 0; i < size; i++) {
                temp[i] = massiv[i];
            }
            delete[] massiv;
            massiv = temp;
            return rez;
        }
        std::cout << "nothing to pop!\n";
        return NULL;
    }


    void push(int x){
        size++;
        int* temp = new int[size];
        for(int i = 0; i<size-1; i++){
            temp[i] = massiv[i];
        }
        temp[size-1] = x;
        delete[] massiv;
        massiv = temp;
    }

    int getSize(){
        return size;
    }
};

int main() {
    Stack st;
    st.push(11);
    st.push(22);
    st.push(33);
    std::cout << st.pop() << "\n";
    st.push(44);
    while(st.getSize()>0){
        std::cout << st.pop() << "\n";
    }
    std::cout << st.pop() << "\n";
    return 0;
}